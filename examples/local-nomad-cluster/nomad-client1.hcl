datacenter = "dc1"

log_level = "DEBUG"

data_dir = "/tmp/nomad-client1"

name = "client1"

bind_addr = "127.0.0.1"

advertise {
  http = "127.0.0.1"
  rpc = "127.0.0.1"
  serf = "127.0.0.1"
}

client {
  enabled = true
  servers = ["127.0.0.1:4647"]
}

ports {
  http = 5656
}

plugin "docker" {
  config {
    gc {
      dangling_containers {
        enabled = false
      }
    }
  }
}
