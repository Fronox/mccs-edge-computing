# Installation
Start nomad server:
```
  nomad agent -config server.hcl
```

Start client1:
```
  mkdir /tmp/client1
  sudo nomad agent -config client1.hcl
```

Start client2:
```
  mkdir /tmp/client2
  sudo nomad agent -config client2.hcl
```

An alternative way to run a standalone nomad instance in `dev` mode (both lient and server on the single node):
```
  sudo nomad agent -dev
```

Also, local consul cluster can be added:
```
  consul agent -dev
```
