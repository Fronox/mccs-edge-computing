datacenter = "dc1"

log_level = "DEBUG"

data_dir = "/tmp/nomad-server1"

name = "server1"

bind_addr = "127.0.0.1"

advertise {
  http = "127.0.0.1"
  rpc = "127.0.0.1"
  serf = "127.0.0.1"
}

server {
  enabled = true

  bootstrap_expect = 1
}
