datacenter = "dc1"
data_dir = "/opt/consul"
retry_join = ["{{ GetInterfaceIP \"ens3\" }}"]
server = true
bootstrap_expect = 1
ui = true
client_addr = "0.0.0.0"
