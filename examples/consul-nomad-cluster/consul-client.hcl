datacenter = "dc1"
data_dir = "/opt/consul"
log_level = "DEBUG"
node_name = "consul-client1"
server = false
start_join = ["consul-server-instance"]
client_addr = "0.0.0.0"
