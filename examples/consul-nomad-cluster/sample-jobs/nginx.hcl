job "nginx" {
  datacenters = ["dc1"]

  update {
    max_parallel = 1
    min_healthy_time = "10s"
    healthy_deadline = "3m"
    progress_deadline = "10m"
    auto_revert = false
    canary = 0
  }

  group "load-balancer" {
    count = 1

    restart {
      attempts = 2
      interval = "30m"
      delay = "20s"
      mode = "fail"
    }

    network {
      port "app" {
        to = 6379
      }
    }

    service {
      name = "${TASKGROUP}-nginx"
      tags = ["global", "load-balancer"]
      port = "app"
    }

    task "nginx" {
      driver = "docker"

      config {
        image = "nginx:1"

        ports = ["app"]
      }

      resources {
        cpu = 500
        memory = 300
      }
    }
  }
}