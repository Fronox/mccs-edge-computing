# Installation
Cluster diagram:
![Cluster](/images/consul-nomad-cluster.png)

* Normal arrows mean connection trough the network
* Dotter arrows mean connection trough Consul

Start consul server (on Node 1):

```
  mkdir /opt/consul
  consul agent -config-file consul-server.hcl -ui
```

Start consul client (on Nodes 2, 3 and 4):
```
  mkdir /opt/consul
  consul agent -config-file consul-client.hcl
```

Start nomad server (on Node 2):
```
  mkdir /opt/nomad
  nomad agent -config nomad-server.hcl
```

Start nomad client (on Nodes 3 and 4):
```
  mkdir /opt/nomad
  nomad agent -config nomad-client.hcl
``` 
