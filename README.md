# Containers Orchestration in Edge Computing Environment

## Description
This is a repository for Mobile and Cloud Computing Seminar project `Containers Orchestration in Edge Computing Environment`.

**Student**: Sergei Zaiaev

**Supervisor**: Chinmaya Dehury

**Coordinator**: Pelle Jakovits

## Background
Nowadays, a lot of software solutions are based on Cloud Computing technology. However, this paradigm has several drawbacks the most important are: 
- centralization of the system: usually several datacenters have to process enormous amounts of unstructured streaming data from several thousands of clients (IoT devices)
- geographical distance from clients, which causes high network load
- data transfer latencies, which cause failures in case of critical situations, when a device requires immediate decisions

Meanwhile, companies want:
- their systems to be WAN independence, due to data privacy regulations
- to have control over the computation
- not rely on distance communication

Possible solution: move important computation closer to clients, which can be implemented with Edge Computing.
## Technical details 
The big picture of Edge Computing is shown below:
![EC: big picture](images/diagram-edge-computing.png)

Image source: [link](https://www.theoryofcomputation.co/what-is-edge-computing/)

There are three main layers:
1. IoT devices - data producers
2. Edge layer, which consists of:
    
    2.1 Edge nodes - small workers near-paced to IoT devices
    
    2.2 Edge gateways, which is used for communication between edge nodes and cloud layer

3. Cloud layer, which is used for heavy computations without strict time constraints.


One of the possible ways to implement this approach is using containers and some lightweight orchestration system for them.

List of the tools used in this project:
- [Docker](https://www.docker.com/) - for containerization
- [Nomad](https://www.nomadproject.io/) - for containers management 
- [Consul](https://www.consul.io/) - for service discovery

## Configuration
The examples require next programs for starting:
- [Nomad](https://www.nomadproject.io/docs/install)
- [Consul](https://www.consul.io/docs/install)

Each example has its own installation instructions.

Instructions for jobs-submitting: [doc](/docs/jobs-submitting.md)
