## Instructions for job submitting

Sumbit a job to a Nomad cluster (run in Nomad server instance):
```
  nomad job run <path_to_job_file>
```

For example, run next command to start Redis job:
```
  nomad job run examples/local-nomad-cluster/sample-jobs/redis.hcl
```

Plan a job in a Nomad cluster (dry run):
```
  nomad job plan <path_to_job_file>
```

For example, run next command to plan updated Redis job:
```
  nomad job plan examples/local-nomad-cluster/sample-jobs/redis.hcl
```

Stop a running job:
```
  nomad job run <job_name>
```

For example, run next command to stop Redis job:
```
  nomad job stop redis
```

`Note`: <job_name> is the name of job in config file:
```
  job "redis" { ...
```
